import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:news_poc_flutter/Landing/bloc/home_bloc.dart';
import 'Landing/views/landing_page.dart';
import 'package:http/http.dart' as http;

class NewsPocApp extends StatelessWidget {
  const NewsPocApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight
    ]);

    return BlocProvider(
        create: (_) => HomeBloc(httpClient: http.Client()),
        child: CupertinoApp(
          initialRoute: LandingPage.routeName,
          routes: {
            LandingPage.routeName: (context) => const LandingPage(),
          },
          theme: const CupertinoThemeData(primaryColor: Colors.red),
        ));
  }
}
