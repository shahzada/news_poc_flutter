part of 'home_bloc.dart';

enum HomePageStatus { loading, success, failure }

class HomeState extends Equatable {
  final String currentURL;
  final HomePageStatus status;
  final bool showMenu;
  final String menuURL = "https://flutter.dev";

  const HomeState(
      {this.status = HomePageStatus.loading,
      this.currentURL = 'https://www.tagesanzeiger.ch/',
      this.showMenu = false});

  HomeState copyWith(
      {HomePageStatus? status, String? currentURL, bool? showMenu}) {
    return HomeState(
        status: status ?? this.status,
        currentURL: currentURL ?? this.currentURL,
        showMenu: showMenu ?? this.showMenu);
  }

  @override
  String toString() {
    return '''PostState { status: $status, currentURL: ${currentURL.toString}, showMenu: $showMenu }''';
  }

  @override
  List<Object> get props => [status, currentURL, showMenu];
}
