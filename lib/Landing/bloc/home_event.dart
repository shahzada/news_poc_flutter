part of 'home_bloc.dart';

enum EventTarget { home, menu }

abstract class HomeEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class InjectScript extends HomeEvent {
  final String script;
  InjectScript(this.script);
}

class ToggleMenu extends HomeEvent {}

class ReloadPage extends HomeEvent {
  ReloadPage(this.target);
  final EventTarget target;
}

class InjectJS extends HomeEvent {
  InjectJS({required this.target});
  final EventTarget target;
}
