import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:http/http.dart' as http;
import 'package:news_poc_flutter/Data/scripts.dart';
import 'package:webview_flutter/webview_flutter.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  WebViewController? _mainViewController, _menuViewController;
  final http.Client httpClient;

  set mainWebViewController(WebViewController controller) =>
      _mainViewController = controller;
  set menuWebcontroller(WebViewController controller) =>
      _menuViewController = controller;

  _controllerForTarget(EventTarget target) {
    switch (target) {
      case EventTarget.home:
        return _mainViewController;
      case EventTarget.menu:
        return _menuViewController;
    }
  }

  HomeBloc({required this.httpClient}) : super(const HomeState()) {
    on<ToggleMenu>(
        (event, emit) => emit(state.copyWith(showMenu: !state.showMenu)));
    on<ReloadPage>((event, emit) {
      _controllerForTarget(event.target)?.reload();
    });
    on<InjectJS>((event, emit) {
      _injectJS(ScriptType.login, _controllerForTarget(event.target)!);
    });
  }

  void _injectJS(ScriptType scriptType, WebViewController controller) async {
    final script = Scripts().script(scriptType);
    String injectionResult =
        await controller.runJavascriptReturningResult(script);
    debugPrint("----------- Injected following script-------------");
    debugPrint(script);
    debugPrint("----------- Result -------------");
    debugPrint(injectionResult);
    debugPrint("------------- End ---------------");
  }
}
