import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:news_poc_flutter/Landing/bloc/home_bloc.dart';
import 'package:webview_flutter/webview_flutter.dart';

class LandingPage extends StatefulWidget {
  static const routeName = "/";
  static const double _sideMenuWidth = 250;

  const LandingPage({Key? key}) : super(key: key);

  @override
  _LandingPage createState() => _LandingPage();
}

class _LandingPage extends State<LandingPage> {
  bool _mainPageSpinnerActive = true;
  bool _menuSpinnerActive = true;
  bool _isDrawerOpen = false;

  @override
  Widget build(BuildContext context) {
    return Stack(children: <Widget>[
      Container(child: _mainView()),
      AnimatedPositioned(
        duration: const Duration(milliseconds: 300),
        curve: Curves.easeInOut,
        top: 0.0,
        right: _isDrawerOpen
            ? MediaQuery.of(context).size.width - LandingPage._sideMenuWidth
            : MediaQuery.of(context).size.width,
        bottom: 0.0,
        left: _isDrawerOpen ? 0 : -LandingPage._sideMenuWidth,
        child: _sideMenuView(),
      ),
      Positioned(
        top: 30,
        left: -10.0,
        // right: 0.0,
        child: SizedBox(
          width: 100,
          child: GestureDetector(
              onTap: () {
                setState(() => _isDrawerOpen = !_isDrawerOpen);
              },
              child: const Icon(
                Icons.menu,
                size: 30,
              )),
        ),
      ),
    ]);
  }

  Widget _mainView() {
    return CupertinoPageScaffold(
        navigationBar: CupertinoNavigationBar(
          previousPageTitle: "Select Image",
          trailing: CupertinoButton(
            child: const Icon(CupertinoIcons.line_horizontal_3),
            padding: EdgeInsets.zero,
            onPressed: () {
              debugPrint("Menu pressed");
              setState(() {
                _isDrawerOpen = !_isDrawerOpen;
              });
              // context.read<HomeBloc>().add(ReloadPage(EventTarget.home));
            },
          ),
          middle: const Text('POC News App'),
        ),
        child: _contentView());
  }

  Widget _sideMenuView() {
    return BlocBuilder<HomeBloc, HomeState>(builder: (context, state) {
      return Stack(children: [
        WebView(
            initialUrl: state.menuURL,
            onWebViewCreated: (controller) {
              debugPrint("@@@ content webview created");
              context.read<HomeBloc>().menuWebcontroller = controller;
            },
            onPageFinished: (_) => setState(() {
                  _menuSpinnerActive = false;
                })),
        Center(
            child: _menuSpinnerActive
                ? const CupertinoActivityIndicator(radius: 20)
                : null)
      ]);
    });
  }

  Widget _contentView() {
    return BlocBuilder<HomeBloc, HomeState>(builder: (context, state) {
      return SafeArea(
          child: Stack(children: [
        WebView(
            initialUrl: state.currentURL,
            onWebViewCreated: (controller) {
              debugPrint("@@@ menu webview created");
              context.read<HomeBloc>().mainWebViewController = controller;
            },
            onPageFinished: (_) => setState(() {
                  _mainPageSpinnerActive = false;
                })),
        Center(
            child: _mainPageSpinnerActive
                ? const CupertinoActivityIndicator(radius: 20)
                : null)
      ]));
    });
  }
}
