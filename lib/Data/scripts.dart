enum ScriptType { pageLoaded, login, logout }

class Scripts {
  static const login = "";
  static const logout = "";
  static const pageLoaded = "";

  String script(ScriptType scriptType) {
    switch (scriptType) {
      case ScriptType.pageLoaded:
        return pageLoaded;

      case ScriptType.login:
        return login;

      case ScriptType.logout:
        return logout;
    }
  }
}
