import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:news_poc_flutter/news_app.dart';
import 'package:news_poc_flutter/simple_bloc_observer.dart';

void main() {
  BlocOverrides.runZoned(
    () {
      runApp(const NewsPocApp());
    },
    blocObserver: SimpleBlocObserver(),
  );
}
